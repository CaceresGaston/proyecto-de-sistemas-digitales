/* Motor Paso a Paso ajustado a grados
 by: www.elprofegarcia.com
   
Gira los grados que se le indiquen por el monitor serial o bluetooth

Arduino    Driver ULN200
  8          IN1
  9          IN2
  10         IN3
  11         IN4
  
Tienda en Linea: http://dinastiatecnologica.com/
*/
#ifndef _MOTORPAP
#define _MOTORPAP

class MotorPaP{
  public:
    MotorPaP(int,int,int,int);
    void ControlMotor(int);
  private:
    void paso_der();
    void paso_izq();
    void apagado();
    int IN1;
    int IN2;
    int IN3;
    int IN4;
    int numero_pasos;
  };

 #endif
