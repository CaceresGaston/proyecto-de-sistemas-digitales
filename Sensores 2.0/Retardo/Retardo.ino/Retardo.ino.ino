
const int ledPin =  13;      // the number of the LED pin
long previousMillis = 0;   
long intervalOn = 500;           
unsigned long currentMillis=0;

void setup() 
{
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite (ledPin, HIGH); 
}

void loop()  {
  
  currentMillis = millis();
  if (digitalRead (ledPin) == HIGH) {
    if(currentMillis - previousMillis > intervalOn) {
      previousMillis = currentMillis;   
      digitalWrite(ledPin, LOW);
      
    Serial.println("Apagar");
   
    }
  }
  if (digitalRead (ledPin) == LOW) {
    if(currentMillis - previousMillis > intervalOn) {
      previousMillis = currentMillis;   
      digitalWrite(ledPin, HIGH);
      
    Serial.println("Prender");
    }
  }
}
