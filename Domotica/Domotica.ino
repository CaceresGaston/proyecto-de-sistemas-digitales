#include <SPI.h>    
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9
int A=0;
int B=0;
MFRC522 mfrc522(SS_PIN, RST_PIN);
byte myCardsA[] = {0x64, 0x5C, 0x36, 0xBB};
byte myCardsB[] = {0x40, 0xFF, 0xC5, 0xDB};
int successRead; 
byte readCard[4];

void setup() 
{
  Serial.begin(9600);                            
  SPI.begin();                                  //Iniciamos protocolo SPI
  mfrc522.PCD_Init();                             
  mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max); 
  Serial.println("IDENTIFIQUESE...");
}

void loop() 
{
  do{
  successRead = getID();
  }
  while (!successRead);
  
  if(readCard[0] == myCardsA[0] && readCard[1] == myCardsA[1] 
  && readCard[2] == myCardsA[2] && readCard[3] == myCardsA[3])            //checking for white card
  {
    Serial.println("Tarjeta");
    A=1;
    B=0;
  }
  if(readCard[0] == myCardsB[0] && readCard[1] == myCardsB[1] 
  && readCard[2] == myCardsB[2] && readCard[3] == myCardsB[3])
  {
     Serial.println("Llavero");
     A=0;
     B=1;
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }

  Serial.println("");
  for (int i = 0; i < 4; i++) {  // 
    readCard[i] = mfrc522.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  
  Serial.println("");
  mfrc522.PICC_HaltA(); 
  return 1;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

