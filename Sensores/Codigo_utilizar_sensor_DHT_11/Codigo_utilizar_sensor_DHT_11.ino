#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <DHT.h>
LiquidCrystal_I2C lcd(0x27,20,4);

DHT dht(12, 11);

int tiempoderefresco = 1500;


void setup()
{
  dht.begin();
  lcd.init();                    
  lcd.backlight();
}


void loop()
{
 lcd.clear(); 
 int temp = dht.readTemperature();
 int humedad = dht.readHumidity();
 lcd.print("TEMPERATURA: ");
 lcd.print(temp);
 lcd.setCursor(0,1);
 lcd.print("HUMEDAD: ");
 lcd.print(humedad);
 delay(tiempoderefresco);
  
}
