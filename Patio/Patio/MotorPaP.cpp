#include "MotorPaP.h"
#include <arduino.h>
#define retardo 5
MotorPaP::MotorPaP(int in1,int in2,int in3, int in4){
  IN1=in1;
  IN2=in2;
  IN3=in3;
  IN4=in4;
  pinMode(IN4, OUTPUT);    // Pin IN4 conectar a IN4
  pinMode(IN3, OUTPUT);    // Pin IN3 conectar a IN3
  pinMode(IN2, OUTPUT);     // Pin IN2 conectar a IN2
  pinMode(IN1, OUTPUT);     // Pin IN1 conectar a IN1
  numero_pasos=0;
  }

void MotorPaP::ControlMotor(int dato_rx){
   

   while (dato_rx>numero_pasos){   // Girohacia la izquierda en grados
       paso_izq();
       numero_pasos = numero_pasos + 1;
   }
   while (dato_rx<numero_pasos){   // Giro hacia la derecha en grados
        paso_der();
        numero_pasos = numero_pasos -1;
   }
 
  apagado();         // Apagado del Motor para que no se caliente
  }
void MotorPaP::paso_der(){         // Pasos a la derecha
 digitalWrite(IN4, LOW); 
 digitalWrite(IN3, LOW);  
 digitalWrite(IN2, HIGH);  
 digitalWrite(IN1, HIGH);  
   delay(retardo); 
 digitalWrite(IN4, LOW); 
 digitalWrite(IN3, HIGH);  
 digitalWrite(IN2, HIGH);  
 digitalWrite(IN1, LOW);  
   delay(retardo); 
 digitalWrite(IN4, HIGH); 
 digitalWrite(IN3, HIGH);  
 digitalWrite(IN2, LOW);  
 digitalWrite(IN1, LOW);  
  delay(retardo); 
 digitalWrite(IN4, HIGH); 
 digitalWrite(IN3, LOW);  
 digitalWrite(IN2, LOW);  
 digitalWrite(IN1, HIGH);  
  delay(retardo);  
}

void MotorPaP::paso_izq() {        // Pasos a la izquierda
 digitalWrite(IN4, HIGH); 
 digitalWrite(IN3, HIGH);  
 digitalWrite(IN2, LOW);  
 digitalWrite(IN1, LOW);  
  delay(retardo); 
 digitalWrite(IN4, LOW); 
 digitalWrite(IN3, HIGH);  
 digitalWrite(IN2, HIGH);  
 digitalWrite(IN1, LOW);  
  delay(retardo); 
 digitalWrite(IN4, LOW); 
 digitalWrite(IN3, LOW);  
 digitalWrite(IN2, HIGH);  
 digitalWrite(IN1, HIGH);  
  delay(retardo); 
 digitalWrite(IN4, HIGH); 
 digitalWrite(IN3, LOW);  
 digitalWrite(IN2, LOW);  
 digitalWrite(IN1, HIGH);  
  delay(retardo); 
}
        
void MotorPaP::apagado() {         // Apagado del Motor
 digitalWrite(IN4, LOW); 
 digitalWrite(IN3, LOW);  
 digitalWrite(IN2, LOW);  
 digitalWrite(IN1, LOW);  
 }

