#include "MotorPaP.h"

int pinLDR = A3;// Pin analogico de entrada para el LDR
int valorLDR = 0;// Variable donde se almacena el valor del LDR
int led = 13;
int porton = 7;
int EmisorReceptor=3;        // emisor receptor
int FinalCarreraC=4;      // Final de carrera cerrar
int FinalCarreraA=5;      //Final de carrera abrir
int angulo=0;
MotorPaP Motor1(8,9,10,11);

void setup() {
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  pinMode(pinLDR,INPUT);
  pinMode(EmisorReceptor,INPUT);
  pinMode(FinalCarreraC,INPUT);
  pinMode(FinalCarreraA,INPUT);
  pinMode(porton,INPUT);
}

void loop() {
  //digitalWrite(led, HIGH);
  if(analogRead(pinLDR)<300){
    digitalWrite(led, HIGH);
  }
  if(analogRead(pinLDR)<550){
    digitalWrite(led,LOW);
  }


  
  if(digitalRead(porton)==HIGH){
    Serial.println("Quiero abrir");
  
   while(digitalRead(FinalCarreraA)==HIGH){
  
   Abrir();
   }
  }
  else {
    while(digitalRead(EmisorReceptor)==HIGH and digitalRead(FinalCarreraC)==HIGH){
     Cerrar();
    }
  }
  
  if(digitalRead(EmisorReceptor)==LOW){
     while(digitalRead(FinalCarreraA)==HIGH){
     
      Abrir();
     }
  }

}
////////////////////////////////////////////////////////////////////////////////////
void Cerrar(){
  angulo=angulo+5;
  Motor1.ControlMotor(angulo);
  Serial.println( angulo);
  Serial.println("Cerrar");  
}
//////////////////////////////////////////////////////////////////////////////////
void Abrir(){
  angulo=angulo-5;
  Motor1.ControlMotor(angulo);
  Serial.println(angulo);
  Serial.println("Abrir");
}

