
#ifndef _Retardo
#define _Retardo

class Retardo{
  
  public:
    Retardo(int);
    int Delay(int, int);
  
  private:
    
    long previousMillis=0;  ////////////////////////////////////////////////
    long intervalOn;      
    unsigned long currentMillis;
  };

 #endif
