//Posible futuro error: Puede llegar a desvordar el angulo antes de que cierre el porton.
//Probar con "+5" del contador
#include <SPI.h>    
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9
#include "MotorPaP.h"
MFRC522 mfrc522(SS_PIN, RST_PIN);
 

byte myCardsA[] = {0x64, 0x5C, 0x36, 0xBB};
byte myCardsB[] = {0xA4, 0xB9, 0x36, 0xBB}; //A4B936BB
byte readCard[4];
int LedGarage=8;
int EmisorReceptor=3;        // emisor receptor
int FinalCarreraC=4;      // Final de carrera cerrar
int FinalCarreraA=5;      //Final de carrera abrir
int SensorDelanterodelAuto=0;        
int SensorTraserodelAuto=0;        
int angulo=0;
MotorPaP Motor1(A1,A2,A3,A4);

void setup() {
  Serial.begin(9600);                            
  SPI.begin(); //Iniciamos protocolo SPI
  mfrc522.PCD_Init();                            
  mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max); 
  
  pinMode(LedGarage,OUTPUT);
  pinMode(EmisorReceptor,INPUT);
  pinMode(FinalCarreraC,INPUT);
  pinMode(FinalCarreraA,INPUT);

}

void loop() {

  EmisorReceptor = digitalRead (3);
  FinalCarreraC = digitalRead (4);
  FinalCarreraA = digitalRead (5);  
 
  

     while(( EmisorReceptor==1)and(FinalCarreraC==1)){
      digitalWrite(LedGarage, HIGH);
      Cerrar();
      EmisorReceptor = digitalRead (3);
      FinalCarreraC = digitalRead (4);
      SensorDelanterodelAuto=0;
      SensorTraserodelAuto=0;
      }
  
  if(EmisorReceptor==0){
    FinalCarreraA=digitalRead(5);
    while(FinalCarreraA==1){
      digitalWrite(LedGarage, HIGH);
      Abrir();
      FinalCarreraA=digitalRead(5);
      }
     }
 LeerSensorDelanterodelAutoySensorTraserodelAuto();
 
 if (SensorDelanterodelAuto==1){
  FinalCarreraA=digitalRead(5);
    while(FinalCarreraA==1){
      digitalWrite(LedGarage, HIGH);
      Abrir();
      FinalCarreraA=digitalRead(5);
    }
   LeerSensorDelanterodelAutoySensorTraserodelAuto();

   while (SensorTraserodelAuto==0){
    LeerSensorDelanterodelAutoySensorTraserodelAuto();
   }

  }
else{
  if(SensorTraserodelAuto==1){
    FinalCarreraA=digitalRead(5);
    while(FinalCarreraA==1){
      digitalWrite(LedGarage, HIGH);
      Abrir();
      FinalCarreraA=digitalRead(5);

    }
    LeerSensorDelanterodelAutoySensorTraserodelAuto();
    while(SensorDelanterodelAuto==0){
      LeerSensorDelanterodelAutoySensorTraserodelAuto();
    }

  }
  else {
    
  }
    
}
  digitalWrite(LedGarage, LOW);
    
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void Cerrar(){
  angulo=angulo+10;
  Motor1.ControlMotor(angulo);
  Serial.println(angulo);
  Serial.println("Cerrar");
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void Abrir(){
  angulo=angulo-10;
  Motor1.ControlMotor(angulo);
  Serial.println( angulo);
  Serial.println("Abrir");
  }
 ///////////////////////////////////////////////////////////////////////////////////////////////////
void LeerSensorDelanterodelAutoySensorTraserodelAuto(){

  Serial.println("IDENTIFIQUESE...");

  getID();
  if(readCard[0] == myCardsA[0] && readCard[1] == myCardsA[1] 
  && readCard[2] == myCardsA[2] && readCard[3] == myCardsA[3])            //checking for white card
  {
    Serial.println("Tarjeta");
    SensorDelanterodelAuto=1;
    SensorTraserodelAuto=0;
  }
  if(readCard[0] == myCardsB[0] && readCard[1] == myCardsB[1] 
  && readCard[2] == myCardsB[2] && readCard[3] == myCardsB[3])
  {
     Serial.println("Llavero");
    SensorDelanterodelAuto=0;
     SensorTraserodelAuto=1;
  }
    for (int i = 0; i < 4; i++) {  // 
    readCard[i] = 0;
    }
}
 ///////////////////////////////////////////////////////////////////////////////////////////////////
int getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }

  Serial.println("");
  for (int i = 0; i < 4; i++) {  // 
    readCard[i] = mfrc522.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  
  Serial.println("");
  mfrc522.PICC_HaltA(); 
  return 1;
}
 ///////////////////////////////////////////////////////////////////////////////////////////////////


